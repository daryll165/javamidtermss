/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package number4;

/**
 *
 * @author 2ndyrGroupA
 */
public class Invoice {
    private int id;
    private Customer customer;
    private double amount;
    public Invoice(int id,Customer customer,double amount){
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }
    public int getID(){
        return id;
    }
    public Customer getCustomer(){
        return customer;
    }
    public void setCustomer(Customer customer){
        this.customer = customer;
    }
    public double getAmount(){
        return amount;
    }
    public void setAmount(double amount){
        this.amount = amount;
    }
    public int getCustomerID(){
        return customer.id;
    }
    public String getCustomerName(){
        return customer.name;
    }
    public int getCustomerDiscount(){
        return customer.discount;
    }
    public double getAmountAfterDiscount(){
        return amount-(amount*(customer.discount*0.01));
    }
    @Override
    public String toString(){
        return "Invoice[id="+id+",customer = "+getCustomer()+",amount = "+amount+"]";
    }
}
