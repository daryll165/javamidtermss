/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package number4;

/**
 *
 * @author 2ndyrGroupA
 */
public class Customer {
    int id;
    String name;
    int discount;
    public Customer(int id,String name,int discount){
        this.id = id;
        this.name = name;
        this.discount = discount;
    }
    public int getID(){
        return id;
    }
    public String getName(){
        return name;
    }
    public int getDiscount(){
        return discount;
    } 
    public void setDiscount(int discount){
        this.discount = discount;
    }
    @Override
    public String toString(){
        return name+"id=("+id+")discount("+discount+"%)";
    }
}
