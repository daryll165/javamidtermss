/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Number2;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author 2ndyrGroupA
 */
public class Tester {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {
        // TODO code application logic her//
        Customer customer1 = new Customer("Daryll");
        System.out.println(customer1);
        System.out.println(customer1.getName());
        System.out.println(customer1.isMember());
        System.out.println(customer1.getMemberType());
        System.out.println(customer1.hashCode());
        
        
        
        String filterDate = "2016-07-31";
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormatter.parse(filterDate);
        DiscountRate discount1 = new DiscountRate();
        System.out.println(discount1.hashCode());
        
        
        
        Visit visit1 = new Visit("Daryll",date);
        System.out.println(visit1.getName());
        System.out.println(visit1.getProductExpense());
        System.out.println(visit1.getServiceExpense());
        System.out.println(visit1.getTotalExpense());   
    }

}
