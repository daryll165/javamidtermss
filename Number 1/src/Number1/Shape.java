/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Number1;

/**
 *
 * @author 2ndyrGroupA
 */
public class Shape {

    private String color;
    private boolean filled;

    public Shape() {
        color = "green";
        filled = true;
    }

    public Shape(String color1, boolean filled1) {
        color = color1;
        filled = filled1;
    }
    // A public method for retrieving the radius

    public String getColor() {
        return color;
    }

    // A public method for computing the area of Shape
    void setColor(String colorSetIn) {
        color = colorSetIn;
    }

    // A public method for retrieving the radius
     boolean isFilled() {
        if (filled == true) {
            return true;
        } else {
            return false;
        }
    }
    // A public method for computing the area of Shape

    void setFilled(boolean filledSetIn) {
        filled = filledSetIn;
    }

    /**
     * Create a new square at default position with default color.
     * @return 
     */
    @Override
    public String toString() {
        String notA = "";
        if (isFilled() == false) {
            notA = "not";
        }
        return "A Shape with a color of " + color + " and is " + notA + " filled. ";
    }
}
