/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Number1;

/**
 *
 * @author 2ndyrGroupA
 */
public class Rectangle extends Shape {

    private double width;
    private double length;

    public Rectangle() {
        super();
        width = 1.0;
        length = 1.0;

    }

    public Rectangle(double width, double length) {
        super();
        this.width = width;
        this.length = length;

    }

    public Rectangle(double width, double length, String color, boolean filled) {
        super(color, filled);
        this.width = width;
        this.length = length;

    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public double getArea() {
        return width * length;
    }

    public double getPerimeter() {
        return (2 * width) + (2 * length);
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "A Rectangle with length of = " + length + " and a width of = " + width + ", which is a subclass of " + super.toString();
    }
}
