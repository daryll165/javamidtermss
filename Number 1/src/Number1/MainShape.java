/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Number1;

/**
 *
 * @author 2ndyrGroupA
 */
public class MainShape {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Shape shape1 = new Shape();
        System.out.println(shape1);


        Square square1 = new Square();
        System.out.println(square1);

        Rectangle rectangle1 = new Rectangle();
        System.out.println(rectangle1);

        Circle circle2 = new Circle(1.1);
        System.out.println(circle2);  
        Circle circle3 = new Circle(); 
        System.out.println(circle3);

        circle2.setRadius(2.2);
        System.out.println(circle2);
        System.out.println("The radius is: " + circle2.getRadius());

        System.out.printf(" The area is: %.2f%n", circle2.getArea());
    }

}
